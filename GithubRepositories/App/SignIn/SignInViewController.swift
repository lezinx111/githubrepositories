//
//  SignInViewController.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit
import OAuthSwift

class SignInViewController: BaseViewController {
    
    // MARK: UI
    
    let signInButton: UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .black
        button.setImage(#imageLiteral(resourceName: "githubIcon"), for: .normal)
        button.setTitle("Authorize", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    // MARK: Auth
    
    let githubAuth = GithubAuthService()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        bindSignInButton()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private methods
    
    private func configureView() {
        title = "Sign in"
        view.backgroundColor = .white
        view.addSubview(signInButton)
        signInButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        signInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    private func bindSignInButton() {
        signInButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.githubAuth.authorize() { result in
                    switch result {
                        case .success(let token):
                            // TODO: Consider saving into database or keychain
                            UserDefaults.standard.set(token, forKey: "apiToken")
                            let vc = DashboardViewController()
                            if let application = UIApplication.shared.delegate as? AppDelegate {
                                application.window?.rootViewController = vc
                            }
                        case .failure(let error):
                            let alert = UIAlertController(title: "Error",
                                                          message: error.localizedDescription,
                                                          preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self?.present(alert, animated: true)
                    }
                    
                }
            }).disposed(by: rx.bag)
    }
}
