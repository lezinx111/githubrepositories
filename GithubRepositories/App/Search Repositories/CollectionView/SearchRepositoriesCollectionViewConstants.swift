//
//  SearchRepositoriesCollectionViewConstants.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit

struct SearchRepositoriesCollectionViewConstants {
    
    static let sectionInsets = UIEdgeInsets(top: 20,
                                            left: 20,
                                            bottom: 20,
                                            right: 20)
}
