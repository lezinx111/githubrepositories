//
//  SearchRepositoriesCollectionViewCell.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit
import Kingfisher

class SearchRepositoriesCollectionViewCell: UICollectionViewCell {
    
    enum CellType {
        case search
        case history
    }
    
    // MARK: UI
    
    private let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let repositoryLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.numberOfLines = 3
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        contentView.addSubview(avatarImageView)
        contentView.addSubview(repositoryLabel)
        NSLayoutConstraint.activate([
            avatarImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            avatarImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            avatarImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            repositoryLabel.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: 10),
            repositoryLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            repositoryLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            repositoryLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        repositoryLabel.setContentCompressionResistancePriority(.init(1000), for: .vertical)
        repositoryLabel.setContentHuggingPriority(.init(rawValue: 100), for: .vertical)
        
    }
    
    func configureView(model: GithubRepository, cellType: CellType = .search) {
        repositoryLabel.text = model.fullName
        let placeholderImage = #imageLiteral(resourceName: "placeholder")
        if let stringUrl = model.owner.avatarUrl, let url = URL(string: stringUrl) {
            avatarImageView.kf.setImage(with: url, placeholder: placeholderImage)
        } else {
            avatarImageView.image = placeholderImage
        }
        if cellType == .search {
            contentView.alpha = model.seen ? 0.5 : 1
        }
    }
}
