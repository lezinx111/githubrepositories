//
//  SearchRepositoriesViewModel.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import RxSwift
import RxCocoa
import Action

class SearchRepositoriesViewModel: BaseViewModel {
    
    let repositories: BehaviorRelay<[GithubRepository]>
    let repositoryPaginationService: RepositoryPaginationService
    let state: BehaviorRelay<RepositoryPaginationService.State>
    let actionSearch = PublishSubject<String>()
    let actionPage = PublishSubject<Void>()
    
    override init() {
        self.repositoryPaginationService = RepositoryPaginationService()
        self.repositories = repositoryPaginationService.items
        self.state = repositoryPaginationService.state
        super.init()
        bindSearching()
    }
    
    private(set) lazy var actionSeen = Action<GithubRepository, GithubRepository> { repository in
        var repository = repository
        repository.seen = true
        repository.dateAdded = Date()
        Database.realmService.githubRepository.setSeen(object: repository)
        return Observable.just(repository)
    }
    
    private func bindSearching() {
        actionSearch
            .subscribe(onNext: { [weak self] in self?.repositoryPaginationService.search(search: $0) })
            .disposed(by: rx.bag)
        
        actionPage
            .subscribe(onNext: { [weak self] in self?.repositoryPaginationService.page() })
            .disposed(by: rx.bag)
    }
}
