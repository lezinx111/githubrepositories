//
//  SearchRepositoriesViewController.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import RxAnimated

class SearchRepositoriesViewController: BaseViewController {
    
    // MARK: UI
    
    lazy var collectionView: LoadingCollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = LoadingCollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        cv.loadingDelegate = self
        cv.delegate = self
        cv.register(SearchRepositoriesCollectionViewCell.self)
        return cv
    }()
    
    let emptyStateView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.alpha = 0
        return view
    }()
    
    let emptyStateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "No repositories..."
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: Properties
    
    private let searchSubject = BehaviorSubject<String>(value: "")
    private let viewModel = SearchRepositoriesViewModel()
    private var collectionViewBottomConstraint: NSLayoutConstraint!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        bindCollectionView()
        bindSearching()
        bindEmptyState()
        bindKeyboard(collectionView,
                     scrollViewBottomConstraint: collectionViewBottomConstraint)
    }
    
    // MARK: Private methods
    
    private func configureView() {
        title = "Repositories"
        configureSearchController()
        configureCollectionView()
        configureEmptyStateView()
    }
    
    private func configureCollectionView() {
        view.addSubview(collectionView)
        let bottomContraint = view.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor)
        self.collectionViewBottomConstraint = bottomContraint
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bottomContraint,
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func configureEmptyStateView() {
        view.addSubview(emptyStateView)
        emptyStateView.addSubview(emptyStateLabel)
        NSLayoutConstraint.activate([
            emptyStateView.topAnchor.constraint(equalTo: view.topAnchor),
            emptyStateView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            emptyStateView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            emptyStateView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            emptyStateLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            emptyStateLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            emptyStateLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            emptyStateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
        ])
        
    }
    private func configureSearchController() {
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search repositories"
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.returnKeyType = .search
        definesPresentationContext = true
    }
    
    // MARK: Bindings
    
    private func bindCollectionView() {
        let dataSource = RxCollectionViewSectionedReloadDataSource<SimpleSection<GithubRepository>>(configureCell: { (ds, cv, ip, model) -> UICollectionViewCell in
            let cell = cv.dequeueReusableCell(SearchRepositoriesCollectionViewCell.self, for: ip)
            cell.configureView(model: model)
            return cell
        })
        
        viewModel.repositories
            .map { [SimpleSection(items: $0)] }
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: rx.bag)
        
        
        collectionView.rx.modelSelected(GithubRepository.self)
            .subscribe(onNext: { [weak self] model in
                self?.viewModel.actionSeen.execute(model)
            }).disposed(by: rx.bag)
        
        viewModel.actionSeen.elements
            .subscribe(onNext: {
                guard let url = URL(string: $0.htmlUrl ?? "") else { return }
                UIApplication.shared.open(url)
            }).disposed(by: rx.bag)
        
        collectionView.rx.itemSelected
            .subscribe({ [weak self] ip in
                if let ip = ip.element {
                    self?.collectionView.reloadItems(at: [ip])
                }
            }).disposed(by: rx.bag)
        
        viewModel.state
            .map { $0 == .loading }
            .bind(to: collectionView.loading)
            .disposed(by: rx.bag)
    }
    
    private func bindSearching() {
        searchSubject
            .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
            .map { $0.trimmed() }
            .filter { $0 != "" }
            .distinctUntilChanged()
            .bind(to: viewModel.actionSearch)
            .disposed(by: rx.bag)
        
        viewModel.state
            .map { $0 == .search }
            .bind(to: searchController.searchBar.rx.isLoading)
            .disposed(by: rx.bag)
        
        viewModel.state
            .subscribe(onNext:  { [weak self] state in
                if case let .error(message) = state {
                    let alert = UIAlertController(title: "Error",
                                                  message: message,
                                                  preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self?.present(alert, animated: true)
                }
     
            }).disposed(by: rx.bag)
    }
    
    private func bindEmptyState() {
        viewModel.repositories
            .map { $0.isEmpty ? CGFloat(1) : 0 }
            .bind(to: emptyStateView.rx.animated.fade(duration: 0.2).alpha)
            .disposed(by: rx.bag)
    }
}

extension SearchRepositoriesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text ?? ""
        searchSubject.onNext(text)
    }
}

extension SearchRepositoriesViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let text = searchController.searchBar.text ?? ""
        searchSubject.onNext(text)
    }
}

extension SearchRepositoriesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow = 2
        let paddingSpace = SearchRepositoriesCollectionViewConstants.sectionInsets.left * (CGFloat(itemsPerRow + 1))
        let availableWidth = view.frame.width - CGFloat(paddingSpace)
        let widthPerItem = availableWidth / CGFloat(itemsPerRow)
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        SearchRepositoriesCollectionViewConstants.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        SearchRepositoriesCollectionViewConstants.sectionInsets.left
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        collectionView.scrollViewDidScroll(scrollView)
    }
}

extension SearchRepositoriesViewController: LoadingCollectionViewDelegate {
    func shouldLoadNewItems(collectionView: LoadingCollectionView) -> Bool {
        !viewModel.repositoryPaginationService.endReached && viewModel.state.value != .loading
    }
    
    func didScrollToBottom(collectionView: LoadingCollectionView) {
        let text = searchController.searchBar.text?.trimmed() ?? ""
        if text.isEmpty {
            return
        }
        viewModel.actionPage.onNext(())
    }
}
