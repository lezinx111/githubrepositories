//
//  DashboardViewController.swift
//  GithubRepositories
//
//  Created by Ziong on 25.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit

class DashboardViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTabs()
        configureView()
    }
    
    private func configureView() {
        tabBar.layer.masksToBounds = false
        tabBar.layer.shadowOffset = CGSize(width: 0.0, height: -3.0)
        tabBar.layer.shadowOpacity = 0.2
        tabBar.layer.shadowRadius = 2.0
    }
        
    // MARK: Private functions
    
    private func initTabs() {
        var viewControllers = [UIViewController]()
        TabKind.allCases
            .forEach {
                let vc = $0.instantiate()
                viewControllers.append(vc)
        }
        self.viewControllers = viewControllers
    }
    
    private func enumerateTabs() -> [TabKind] {
        guard let items = self.tabBar.items else {
            return []
        }
        return items.compactMap { TabKind(rawValue: $0.tag) }
    }
}

enum TabKind: Int, CaseIterable, Comparable {
    static func < (lhs: TabKind, rhs: TabKind) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
    
    case explore
    case history
    
    var title: String {
        switch self {
        case .explore:
            return "Explore"
        case .history:
            return "History"
        }
    }
    
    var tabBarItem: UITabBarItem {
        switch self {
        case .explore:
            return UITabBarItem(tabBarSystemItem: .search, tag: self.rawValue)
        case .history:
            return UITabBarItem(tabBarSystemItem: .history, tag: self.rawValue)
        }
    }
    
    func instantiate() -> UIViewController {
        let vc: UIViewController
        switch self {
        case .explore:
            vc = UINavigationController(rootViewController: SearchRepositoriesViewController())
        case .history:
            vc = UINavigationController(rootViewController: HistoryViewController())
        }
        vc.tabBarItem = self.tabBarItem
        return vc
    }
}

