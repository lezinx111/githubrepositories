//
//  HistoryViewModel.swift
//  GithubRepositories
//
//  Created by Ziong on 25.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import RxSwift
import RxCocoa
import Action
import RealmSwift

class HistoryViewModel: BaseViewModel {
    
    let repositories: BehaviorRelay<[GithubRepository]>
    
    override init() {
        self.repositories = BehaviorRelay(value: [])
        super.init()
        bindItems()
    }
    
    private func bindItems() {
        Database.realmService.githubRepository.rx.allSeen
            .map { Array($0.prefix(20)) }
            .bind(to: repositories)
            .disposed(by: rx.bag)
    }
}
