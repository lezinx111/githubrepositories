//
//  HistoryViewController.swift
//  GithubRepositories
//
//  Created by Ziong on 25.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import RxAnimated

class HistoryViewController: BaseViewController {
    
    // MARK: UI
    
    lazy var collectionView: LoadingCollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = LoadingCollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        cv.delegate = self
        cv.register(SearchRepositoriesCollectionViewCell.self)
        return cv
    }()
    
    let emptyStateView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.alpha = 0
        return view
    }()
    
    let emptyStateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "No repositories..."
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    
    // MARK: Properties
    
    private let viewModel = HistoryViewModel()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        bindCollectionView()
        bindEmptyState()
    }
    
    // MARK: Private methods
    
    private func configureView() {
        title = "History"
        configureEmptyStateView()
        configureCollectionView()
        view.bringSubviewToFront(emptyStateView)
        view.bringSubviewToFront(emptyStateLabel)
    }
    
    private func configureCollectionView() {
        view.addSubview(collectionView)
        view.addSubview(collectionView.indicatorView)
        let bottomContraint = view.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bottomContraint,
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func configureEmptyStateView() {
        view.addSubview(emptyStateView)
        emptyStateView.addSubview(emptyStateLabel)
        NSLayoutConstraint.activate([
            emptyStateView.topAnchor.constraint(equalTo: view.topAnchor),
            emptyStateView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            emptyStateView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            emptyStateView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            emptyStateLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            emptyStateLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            emptyStateLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            emptyStateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
        ])
    }
    
    // MARK: Bindings
    
    private func bindCollectionView() {
        let dataSource = RxCollectionViewSectionedReloadDataSource<SimpleSection<GithubRepository>>(configureCell: { (ds, cv, ip, model) -> UICollectionViewCell in
            let cell = cv.dequeueReusableCell(SearchRepositoriesCollectionViewCell.self, for: ip)
            cell.configureView(model: model, cellType: .history)
            return cell
        })
        
        viewModel.repositories
            .map { [SimpleSection(items: $0)] }
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: rx.bag)
        
        
        collectionView.rx.modelSelected(GithubRepository.self)
            .subscribe(onNext: { model in
                guard let url = URL(string: model.htmlUrl ?? "") else { return }
                UIApplication.shared.open(url)
            }).disposed(by: rx.bag)
    }
    
    private func bindEmptyState() {
        viewModel.repositories
            .map { $0.isEmpty ? CGFloat(1) : 0 }
            .bind(to: emptyStateView.rx.animated.fade(duration: 0.2).alpha)
            .disposed(by: rx.bag)
    }
}


extension HistoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow = 2
        let paddingSpace = SearchRepositoriesCollectionViewConstants.sectionInsets.left * (CGFloat(itemsPerRow + 1))
        let availableWidth = view.frame.width - CGFloat(paddingSpace)
        let widthPerItem = availableWidth / CGFloat(itemsPerRow)
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        SearchRepositoriesCollectionViewConstants.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        SearchRepositoriesCollectionViewConstants.sectionInsets.left
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        collectionView.scrollViewDidScroll(scrollView)
    }
}
