//
//  ActivityIndicatorView.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class ActivityIndicatorView: UIView {

    private(set) var indicator: UIActivityIndicatorView!
    
    @IBInspectable
    private var height: CGFloat = 40
    
    init(height: CGFloat) {
        self.height = height
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height)
        super.init(frame: frame)
        setup()
    }
    
    override init(frame: CGRect) {
        self.height = frame.height
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
        
    private func setup() {
        indicator = UIActivityIndicatorView(style: .gray)
        indicator.center = center
        indicator.autoresizingMask = [
            .flexibleTopMargin,
            .flexibleLeftMargin,
            .flexibleBottomMargin,
            .flexibleRightMargin
        ]
        addSubview(indicator)
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
        clipsToBounds = true
    }
}
