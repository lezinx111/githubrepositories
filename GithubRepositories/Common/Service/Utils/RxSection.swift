//
//  RxSection.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

import Foundation
import RxDataSources

typealias SimpleSection<T> = RxSection<T, Void>

// MARK: RxSection

struct RxSection<T,V> {
    var header: V
    var items: [T]
    
    init(header: V, items: [T] = [T]()) {
        self.header = header
        self.items = items
    }
}

extension RxSection : SectionModelType {
    typealias Item = T
    
    init(original: RxSection<T, V>, items: [T]) {
        self = original
        self.items = items
    }
}

extension RxSection where V == Void {
    init(items: [T] = [T]()) {
        self.init(header: (), items: items)
    }
}
