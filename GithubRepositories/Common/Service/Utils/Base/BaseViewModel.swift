//
//  BaseViewModel.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BaseViewModel {
    deinit {
        print("__DEINIT____\(self)")
    }
}

extension BaseViewModel: ReactiveCompatible {}
