//
//  BaseViewController.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxKeyboard

class BaseViewController: UIViewController {
    deinit {
        print("__DEINIT____\(self)")
    }

}

extension BaseViewController {
    func bindKeyboard(_ scrollView: UIScrollView, scrollViewBottomConstraint: NSLayoutConstraint, searchBar: UISearchBar? = nil, disposeBag: DisposeBag? = nil) {
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [weak self, weak scrollViewBottomConstraint] visibleHeight in
                scrollViewBottomConstraint?.constant = visibleHeight
                self?.view.layoutIfNeeded()
            })
            .disposed(by: disposeBag ?? rx.bag)
        
        scrollView.panGestureRecognizer.rx.event.asControlEvent()
            .subscribe(onNext: { [weak self] pan in
                if pan.velocity(in: self?.view).y > 1500 {
                    if let searchBar = searchBar {
                        searchBar.endEditing(true)
                    } else {
                        self?.view.endEditing(true)
                    }
                }
            }).disposed(by: disposeBag ?? rx.bag)
    }
}
