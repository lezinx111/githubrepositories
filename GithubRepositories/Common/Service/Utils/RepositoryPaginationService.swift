//
//  RepositoryPaginationService.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class RepositoryPaginationService: PaginationServiceProtocol {

    typealias T = GithubRepository
    typealias Request = GetGithubRepositoriesRequest.RequestModel
    
    struct Result {
        let pageInfo: PageInfo
        let items: [T]
        let response: GetGithubRepositoriesRequest.ResponseModel?
        let errorMessage: String?
    }
    
    var endReached: Bool {
        pageInfo.endReached
    }
    
    var items: BehaviorRelay<[GithubRepository]>
    
    var bag: DisposeBag = DisposeBag()
    
    var state: BehaviorRelay<State>
    
    var pageSubject = PublishSubject<PageInfo>()

    var pageInfo: PageInfo
    
    var pageSize: Int = 30
    
    var search = ""

    init() {
        self.pageInfo = PageInfo(offset: 0, limit: pageSize, page: 1, total: nil)
        self.state = BehaviorRelay(value: .list)
        let notSeenPredicate = NSPredicate(format: "%K == %@",
                                        #keyPath(RealmGithubRepository.seen),
                                        NSNumber(booleanLiteral: false))
        let items = Database.realmService.githubRepository.find(where: notSeenPredicate)
        self.items = BehaviorRelay(value: items)
        bindPageSubject()
    }
    
    func search(search: String) {
        self.pageInfo = PageInfo(offset: 0, limit: pageSize, page: 1, total: nil)
        self.search = search
        self.state.accept(.search)
        pageSubject.onNext(pageInfo)
    }
    
    func page() {
        self.state.accept(.loading)
        pageSubject.onNext(pageInfo)
    }
    
    private func didPage(result: Result) {
        if let response = result.response {
            if pageInfo.page == 1 {
                self.items.accept(result.items)
            } else {
                // User is paging
                self.items.accept(self.items.value + result.items)
            }
            self.pageInfo.offset += pageSize
            self.pageInfo.page += 2
            self.pageInfo.total = response.totalCount
            
            self.state.accept(.list)
        } else if let errorMessage = result.errorMessage {
            self.state.accept(.error(errorMessage))
        } else {
            self.state.accept(.list)
        }
    }
    
    private func bindPageSubject() {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 2
        pageSubject
            .observeOn(MainScheduler.instance)
            .flatMapLatest { [weak self] pageInfo -> Observable<Result> in
                guard let self = self else { return .empty() }
                let model = GetGithubRepositoriesRequest.RequestModel(q: self.search,
                                                                      sort: .stars,
                                                                      perPage: pageInfo.limit / 2,
                                                                      page: pageInfo.page)
                let model2 = GetGithubRepositoriesRequest.RequestModel(q: self.search,
                                                                       sort: .stars,
                                                                       perPage: pageInfo.limit / 2,
                                                                       page: pageInfo.page + 1)
                return Observable.zip(GetGithubRepositoriesRequest(model),
                                      GetGithubRepositoriesRequest(model2))
                    .map { ($0.0, $0.1, pageInfo, nil) }
                    .catchError { error -> Observable<(GetGithubRepositoriesRequest.ResponseModel?, GetGithubRepositoriesRequest.ResponseModel?, PageInfo, String?)> in
                        if (error as? ApiError)?.errorType == .offline {
                            return Observable.just(())
                                .delay(.milliseconds(100), scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
                                .map { _ in (nil, nil, pageInfo, "No internet connection")}
                        }
                        return Observable.just((nil, nil, pageInfo, (error as? ApiError)?.errorMessage ))
                }
                .map { (response, resp, pageInfo, errorMessage) in
                    if let response = response,
                       let resp = resp {
                        let items = response.items + resp.items
                        return Result(pageInfo: pageInfo, items: items, response: response, errorMessage: nil)
                    } else {
                        // Fetch from DB
                        return Result(pageInfo: pageInfo, items: [], response: nil, errorMessage: errorMessage)
                    }
                }
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.didPage(result: $0)
            })
            .disposed(by: bag)
    }
}
