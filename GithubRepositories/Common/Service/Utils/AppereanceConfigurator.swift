//
//  AppereanceConfigurator.swift
//  Meow
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit

struct AppereanceConfigurator {
    
    static func configure() {
        configureNavigationBar()
    }
    
    private static func configureNavigationBar() {
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().configureAppearance()
    }
}

extension UINavigationBar {
    func configureAppearance() {
        titleTextAttributes = [.foregroundColor: UIColor.black]
        largeTitleTextAttributes = [.foregroundColor: UIColor.black]
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithDefaultBackground()
            navBarAppearance.backgroundColor = .white
            standardAppearance = navBarAppearance
            scrollEdgeAppearance = navBarAppearance
        } else {
            isTranslucent = false
            barTintColor = .white
            backgroundColor = .white

        }
    }
}
