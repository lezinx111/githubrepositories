//
//  PaginationService.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct PageResponse<T> {
    let limit: Int
    let page: Int
    let total: Int
    let items: [T]
}

protocol PaginationServiceProtocol {
    associatedtype T
    associatedtype Request
    
    typealias State = PaginationState
    
    var endReached: Bool { get }
    var pageSize: Int { get }
    var items: BehaviorRelay<[T]> { get set }
    var pageInfo: PageInfo { get set }
    var bag: DisposeBag { get }
    var state: BehaviorRelay<State> { get }
    var pageSubject: PublishSubject<PageInfo> { get }
}

struct PageInfo: Hashable {
    var offset: Int
    var limit: Int
    var page: Int
    var total: Int?
    
    var endReached: Bool {
        guard let total = total else {
            return false
        }
        return offset > total
    }
}

enum PaginationState: Equatable {
    case loading
    case list
    case error(String)
    case search
}
