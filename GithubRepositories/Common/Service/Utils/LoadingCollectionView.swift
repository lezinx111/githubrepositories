//
//  LoadingCollectionView.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol LoadingCollectionViewDelegate: class {
    func didScrollToBottom(collectionView: LoadingCollectionView)
    func shouldLoadNewItems(collectionView: LoadingCollectionView) -> Bool
}

class LoadingCollectionView: UICollectionView {
    
    deinit {
        print("\nDeallocating - \(self)")
    }
    
    var loaderAnimationDuration: CGFloat = 0.5
    var indicatorHeight: CGFloat = 40 {
        didSet {
            indicatorView = ActivityIndicatorView(height: indicatorHeight)
        }
    }
    weak var loadingDelegate: LoadingCollectionViewDelegate? = nil
    lazy var indicatorView = ActivityIndicatorView(height: indicatorHeight)
    
    let loading = BehaviorRelay(value: false)
    
    private var previousScrollOffset: CGFloat = 0
    private var shouldSkipScrollViewDidScroll = false

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        bindLoading()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func hideActivityIndicator() {
        if indicatorView.indicator.isAnimating {
            indicatorView.indicator.stopAnimating()
        }
        
        let offset = contentOffset
        
        UIView.animate(withDuration: TimeInterval(loaderAnimationDuration)) { [weak self] in
            guard let self = self else {
                return
            }
            self.indicatorView.frame.size.height = 0
            self.shouldSkipScrollViewDidScroll = true
            self.contentOffset = offset
            self.shouldSkipScrollViewDidScroll = false
        }
    }
    
    private func showActivityIndicator() {
        let indicator = indicatorView.indicator!
        
        if !indicator.isAnimating {
            indicator.startAnimating()
        }
        
        UIView.animate(withDuration: TimeInterval(loaderAnimationDuration)) { [weak self] in
            guard let self = self else {
                return
            }
            self.indicatorView.frame.size.height = self.indicatorHeight
        }
    }
    
    private func bindLoading() {
        loading
            .subscribe(onNext: { [weak self] loading in
                loading ? self?.showActivityIndicator() : self?.hideActivityIndicator()
            }).disposed(by: rx.bag)
    }
}

extension LoadingCollectionView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard !shouldSkipScrollViewDidScroll else {
            return
        }
        let scrollDiff = scrollView.contentOffset.y - previousScrollOffset
        previousScrollOffset = scrollView.contentOffset.y
        
        if scrollDiff > 0 && scrollView.contentOffset.y > 0 {
            let topOffset = contentOffset.y
            let topShift = 0.5 * frame.size.height
            let unseenContentHeight = contentSize.height - frame.size.height
            let framePartForPageReached = topOffset + topShift >= unseenContentHeight
            let contentPartForPageReached = topOffset >= 0.75 * contentSize.height
            if let delegate = self.loadingDelegate,
                framePartForPageReached || contentPartForPageReached,
                delegate.shouldLoadNewItems(collectionView: self) {
                self.contentInset.bottom = 75
                loadingDelegate?.didScrollToBottom(collectionView: self)
            } else {
                self.contentInset.bottom = 0
            }
        }
     }
}

