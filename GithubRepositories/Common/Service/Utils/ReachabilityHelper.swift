//
//  ReachabilityHelper.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import Reachability

struct ReachabilityHelper {
    
    static var isOnline: Bool {
        if let reachability = try? Reachability() {
            return reachability.connection != .unavailable
        }
        return false
    }

    private init() {}
}
