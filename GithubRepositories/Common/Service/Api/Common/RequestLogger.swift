//
//  RequestLogger.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

#if DEBUG

import Foundation
import Alamofire

struct RequestLogger {
    
    static func log(request: URLRequest?) {
        guard let request = request else { return }
        var dataString = "<Without body>"
        if var body = request.httpBody {
            if let jsonObject = try? JSONSerialization.jsonObject(with: body, options: []) as? [String: Any],
                let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) {
                body = jsonData
            }
            dataString = String(data: body, encoding: .utf8) ?? "<Body not parsed>"
        }
        
        let headers = (request.allHTTPHeaderFields ?? [:]).map { "\($0): \($1)" }.joined(separator: "\n")
        
        let debugInfo = """
        
        __API_REQUEST ⏩
        \(request.httpMethod ?? "") \(request.url?.absoluteString ?? "") [\(Date())]
        \(headers)
        
        \(dataString)
        
        """
        print(debugInfo)
    }
    
    static func log(response: AFDataResponse<Any>) {
        
        var dataString = "<Without body>"
        if var body = response.data, body.count > 0 {
            if let jsonObject = try? JSONSerialization.jsonObject(with: body, options: []),
                let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) {
                body = jsonData
            }
            dataString = String(data: body, encoding: .utf8) ?? "[Body not parsed]"
        }
        let statusCode = response.response?.statusCode ?? -1
        let headers = (response.response?.allHeaderFields ?? [:]).map { "\($0): \($1)" }.joined(separator: "\n")
        let debugInfo = """
        
        __API_RESPONSE ⏪
        \(statusCode) \(response.request?.httpMethod ?? "") \(response.response?.url?.absoluteString ?? "") [\(Date())]
        \(headers)
        
        \(dataString)
        
        """
        print(debugInfo)
    }
}

#endif
