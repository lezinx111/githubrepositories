//
//  ApiConstants.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

struct ApiConstants {
    static let API_URL = "https://api.github.com"
    
    static let githubConsumerKey = "3b792c5717bd9609ebcc"
    static let githubConsumerSecret = "d1de27a808e0f8154d7d66f787b01a33928e0959"
    static let authorizeUrl = "https://github.com/login/oauth/authorize"
    static let accessTokenUrl = "https://github.com/login/oauth/access_token"
}
