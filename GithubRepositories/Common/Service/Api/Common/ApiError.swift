//
//  ApiError.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

struct ApiError: Error {
    
    static func parse(data: Data?) -> ApiError {
        guard let data = data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: [.allowFragments]),
            let jsonDict = jsonObject as? [String: Any] else {
                return ApiError(errorType: .unknown, errorMessage: nil)
        }
        let apiErrorType: ApiErrorType
        if let errorMessage = jsonDict["message"] as? String {
            return ApiError(errorType: .unknown, errorMessage: errorMessage)
        }
        return ApiError(errorType: .unknown)
    }
    
    let errorType: ApiErrorType
    let errorMessage: String?

    init(errorType: ApiErrorType, errorMessage: String? = nil) {
        self.errorType = errorType
        self.errorMessage = errorMessage
    }
}

enum ApiErrorType {
    case offline
    case unknown
}
