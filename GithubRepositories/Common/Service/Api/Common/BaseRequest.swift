//
//  BaseRequest.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

protocol BaseRequest: ObservableType {
    associatedtype ResponseModel: Decodable
    associatedtype Element = ResponseModel
    var decoder: JSONDecoder { get }
    func sync(_ model: ResponseModel)
    func endpoint() -> Endpoint
}

extension BaseRequest where Element == ResponseModel {
    func subscribe<O: ObserverType>(_ observer: O) -> Disposable where O.Element == Element {
        asObservable().subscribe(observer)
    }
    
    func asObservable() -> Observable<Element> {
        return call()
    }
}

extension BaseRequest {
    
    var decoder: JSONDecoder {
        defaultDecoder
    }
    
    var defaultDecoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }

    func sync(_ model: ResponseModel) {
    }

    func call(
        scheduler: SchedulerType? = MainScheduler.instance,
        taskScheduler: SchedulerType = ConcurrentDispatchQueueScheduler(qos: .background)
    ) -> Observable<ResponseModel> {
        return createAsyncTask(scheduler: scheduler, taskScheduler: taskScheduler) {
            return self.createRequest(taskScheduler: taskScheduler)
        }
    }
    
    
    private func createAsyncTask<O>(
        scheduler: SchedulerType? = MainScheduler.instance,
        taskScheduler: SchedulerType = ConcurrentDispatchQueueScheduler(qos: .background),
        task: @escaping () throws -> O
    ) -> Observable<O.Element> where O: ObservableConvertibleType {
        let observable = Observable.online()
            .flatMap(task)
            .subscribeOn(taskScheduler)
        if let scheduler = scheduler {
            return observable.observeOn(scheduler)
        }
        return observable
    }

    
    private func createRequest(taskScheduler: SchedulerType) -> Observable<ResponseModel> {
        return decodeResponse(sendRequest(taskScheduler: taskScheduler))
    }
    
    private func sendRequest(taskScheduler: SchedulerType) -> Observable<AFDataResponse<Any>> {
        Observable<AFDataResponse<Any>>.create { observer in
            let request = AF.request(self.endpoint())
            request
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    #if DEBUG
                    RequestLogger.log(response: response)
                    #endif
                    observer.on(.next(response))
                    observer.on(.completed)
            }
            return Disposables.create {
                request.cancel()
            }
        }
        .observeOn(taskScheduler)
    }
    
    private func decodeResponse(_ observable: Observable<AFDataResponse<Any>> ) -> Observable<ResponseModel> {
        let decoder = self.decoder
        return observable
            .flatMap { response -> Observable<ResponseModel> in
            do {
                let json = response.data
                switch response.result {
                case .success(_):
                    let model = try decoder.decode(ResponseModel.self, from: json!)
                    self.sync(model)
                    return .just(model)
                case .failure(let error):
                    // TODO: Parse Error
                    print("__\(#function): \(error.localizedDescription)")
                    return .error(ApiError.parse(data: json))
                }
            } catch {
                print("__\(#function): \(error)")
                return .error(ApiError(errorType: .offline))
            }
        }
    }
}
