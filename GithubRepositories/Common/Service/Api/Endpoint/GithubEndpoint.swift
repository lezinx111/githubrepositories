//
//  GithubEndpoint.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Alamofire

enum GithubSearchEndpoint: Endpoint {
    
    case repositories(query: GetGithubRepositoriesRequest.RequestModel)
    
    var path: String {
        switch self {
        case .repositories:
            return "search/repositories"
        }
    }
    
    var headerContentType: String {
        return "application/json"
    }
    
    var params: [String : Any]? {
        switch self {
        case .repositories(let query):
            return query.toDictionary()
        }
    }
    
    var body: Encodable? {
        return nil
    }
    
    var method: HTTPMethod {
        switch self {
        case .repositories:
            return .get
        }
    }
}


