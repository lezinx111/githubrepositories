//
//  Endpoint.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import Alamofire

protocol Endpoint: URLRequestConvertible {
    var path: String { get }
    var params: Parameters? { get }
    var body: Encodable? { get }
    var method: HTTPMethod { get }
    var headerContentType: String { get }
}

extension Endpoint {
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.API_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest = try URLEncoding.default.encode(urlRequest, with: params)
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue(headerContentType, forHTTPHeaderField: "Content-Type")
        if let token = UserDefaults.standard.string(forKey: "apiToken") {
            urlRequest.setValue("token \(token)", forHTTPHeaderField: "Authorization")
        }
        
        if let b = body {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .millisecondsSince1970
            let value = try encoder.encode(EncodableWrapper(b))
            urlRequest.httpBody = value
            print("body: \(String(describing: String(data: value, encoding: .utf8)))")
        }

        print("param: \(String(describing: params)) path: \(path) url: \(String(describing: urlRequest.url))")
        #if DEBUG
        RequestLogger.log(request: urlRequest)
        #endif
        return urlRequest
    }
}
