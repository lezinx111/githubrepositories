//
//  GetGithubRepositoriesRequest.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

class GetGithubRepositoriesRequest: BaseRequest {
    
    private let model: RequestModel
    
    init(_ model: RequestModel) {
        self.model = model
    }
    
    func endpoint() -> Endpoint {
        GithubSearchEndpoint.repositories(query: model)
    }
    
    var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    func sync(_ model: ResponseModel) {
        _ = Database.realmService.githubRepository.createOrUpdate(model.items)
    }
}

extension GetGithubRepositoriesRequest {
    
    struct ResponseModel: Decodable {
        let totalCount: Int
        let items: [GithubRepository]
    }
    
    struct RequestModel: Encodable {
        let q: String
        let sort: Sort
        let perPage: Int
        let page: Int
    }
    
    enum Sort: String, Encodable {
        case stars
        case forks
    }
}
