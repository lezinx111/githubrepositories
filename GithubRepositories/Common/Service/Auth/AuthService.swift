//
//  AuthService.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import OAuthSwift

protocol AuthService {
    var oauthswift: OAuth2Swift { get }
    func authorize(completion: @escaping (Result<String, Error>) -> Void)
}
