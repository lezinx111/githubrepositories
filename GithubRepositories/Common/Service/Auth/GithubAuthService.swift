//
//  GithubAuthService.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import OAuthSwift

class GithubAuthService: AuthService {
    var oauthswift: OAuth2Swift = {
        let oauthswift = OAuth2Swift(
            consumerKey:    ApiConstants.githubConsumerKey,
            consumerSecret: ApiConstants.githubConsumerSecret,
            authorizeUrl:   ApiConstants.authorizeUrl,
            accessTokenUrl: ApiConstants.accessTokenUrl,
            responseType:   "code"
        )

        oauthswift.authorizeURLHandler = oauthswift.authorizeURLHandler
        return oauthswift
    }()
}

extension GithubAuthService {
    func authorize(completion: @escaping (Result<String, Error>) -> Void) {
        let state = generateState(withLength: 20)
        let _ = oauthswift.authorize(
        withCallbackURL: URL(string: "mygithubapp://")!, scope: "user,repo", state: state) { result in
            switch result {
            case .success(let (credential, _, _)):
                completion(.success(credential.oauthToken))
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
        
    }
}
