//
//  Database.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import RealmSwift

class Database {
    
    static let realmService = RealmService()
    
    static func initDefaultValues() {
        _ = realmService
        
        realmService.initDefaultValues()
    }
    
    static func clear() {
        realmService.clear()
    }
}
