//
//  DaoGithubRepository.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift
import RxCocoa
import RxSwift

class DaoGithubRepository: Dao<GithubRepository> {
    typealias K = RealmGithubRepository
    
    override var sortParams: [SortDescriptor] {
        return [
            SortDescriptor(keyPath: #keyPath(K.stargazersCount), ascending: false),
        ]
    }
    
    func clear() {
        let seenPredicate = NSPredicate(format: "%K == %@",
                                        #keyPath(RealmGithubRepository.seen),
                                        NSNumber(booleanLiteral: false))
        _ = delete(where: seenPredicate)
    }
    
    func setSeen(object: GithubRepository) {
        return realm(write: true) { realm in
            guard let old = realm.object(ofType: K.self, forPrimaryKey: object.uid) else {
                return
            }
            old.seen = object.seen
            old.dateAdded = object.dateAdded
            realm.add(old, update: .all)
        }
    }
    
}

extension Reactive where Base: DaoGithubRepository {
    var allSeen: Observable<[Base.T]> {
        let seenPredicate = NSPredicate(format: "%K == %@",
                                        #keyPath(RealmGithubRepository.seen),
                                        NSNumber(booleanLiteral: true))
        let sort = [SortDescriptor(keyPath: #keyPath(RealmGithubRepository.dateAdded),
                                   ascending: false)]
        return `where`(seenPredicate, sort: sort)
    }
}
