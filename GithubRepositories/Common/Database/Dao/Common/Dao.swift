//
//  Dao.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift

class Dao<T: RealmConvertible>: DaoProtocol {
    
    typealias R = T.RealmObjectType
    
    let lock = Mutex()
    
    let configuration: Realm.Configuration
    var extraPredicates: [NSPredicate]
    var sortParams: [SortDescriptor] {
        return []
    }
    
    required init(_ configuration: Realm.Configuration, extraPredicates: [NSPredicate]) {
        self.configuration = configuration
        self.extraPredicates = extraPredicates
    }
    
    func initDefaultValues() {}
    
    // MARK: - Create or update
    
    func createOrUpdate(_ object: T, write: Bool = true) -> Error? {
        return realm(write: write) {
            try $0.add(object, update: .all)
        }
    }
    
    func createOrUpdate(_ objects: [T], write: Bool = true) -> Error? {
        return realm(write: write) {
            try $0.add(objects, update: .all)
        }
    }
    
    // MARK: - Find
    
    func findOne(uid: T.Uid, write: Bool = false) -> T? {
        guard R.primaryKey() != nil else { return nil }
        return realm(or: nil) {
            if let realmObject = $0.object(ofType: R.self, forPrimaryKey: uid) {
                return try T(realm: realmObject)
            }
            return nil
        }
    }
    
    func findOne(where predicate: NSPredicate? = nil,
                 sortBy sortDescriptors: [SortDescriptor]? = nil,
                 write: Bool = false) -> T? {
        let sortDescriptors = sortDescriptors ?? sortParams
        return realm(or: nil, write: write) {
            let q = query($0, where: predicate.andPredicates(extraPredicates))
                .sort(by: sortDescriptors)
            if let first = q.first() {
                return try T(realm: first)
            }
            else {
                return nil
            }
        }
    }

    func find(where predicate: NSPredicate? = nil,
              sortBy sortDescriptors: [SortDescriptor]? = nil,
              write: Bool = false) -> [T] {
        let sortDescriptors = sortDescriptors ?? sortParams
        return realm(or: [], write: write) {
            let q = query($0, where: predicate.andPredicates(extraPredicates))
                .sort(by: sortDescriptors)
            return q.array().compactMap {
                try? T(realm: $0)
            }
        }
    }
    
    // MARK: - Delete
    
    func deleteOne(_ object: T, write: Bool = true) -> Error? {
        return realm(write: write) {
            if T.RealmObjectType.primaryKey() != nil {
                if let object = $0.object(ofType: T.RealmObjectType.self, forPrimaryKey: object.uid) {
                    $0.delete(object)
                }
            }
            else {
                let realmObject = try object.realmObject()
                $0.delete(realmObject)
            }
        }
    }
    
    func delete(_ objects: [T], write: Bool = true) -> Error? {
        return realm(write: write) {
            if let primaryKey = T.RealmObjectType.primaryKey() {
                let uids = objects.map { $0.uid }
                let objects = $0.objects(T.RealmObjectType.self)
                    .filter("%K IN %@", primaryKey, uids)
                $0.delete(objects)
            }
            else {
                let realmObjects = try objects.realmObjects()
                $0.delete(realmObjects)
            }
        }
    }
    
    func delete(where predicate: NSPredicate? = nil,
                sortBy sortDescriptors: [SortDescriptor]? = nil,
                write: Bool = true) -> Error? {
        let sortDescriptors = sortDescriptors ?? sortParams
        return realm(write: write) {
            let q = query($0, where: predicate.andPredicates(extraPredicates))
                .sort(by: sortDescriptors)
            let realmObjects = q.results()
            $0.delete(realmObjects)
        }
    }
}

extension Array where Element == NSPredicate {
    var andPredicate: NSPredicate? {
        guard !self.isEmpty else {
            return nil
        }
        return NSCompoundPredicate(type: .and, subpredicates: self)
    }
}

extension NSPredicate {
    func andPredicates(_ predicates: [NSPredicate]) -> NSPredicate {
        guard !predicates.isEmpty else {
            return self
        }
        return NSCompoundPredicate(type: .and, subpredicates: [self] + predicates)
    }
}

extension Optional where Wrapped == NSPredicate {
    func andPredicates(_ predicates: [NSPredicate]) -> NSPredicate? {
        if case let .some(predicate) = self {
            return predicate.andPredicates(predicates)
        }
        return predicates.andPredicate
    }
}
