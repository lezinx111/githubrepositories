//
//  DaoProtocol.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxCocoa

protocol AnyDao: class {
    var lock: Mutex { get }
    
    var configuration: Realm.Configuration { get }
    
    var extraPredicates: [NSPredicate] { get }
    
    var sortParams: [SortDescriptor] { get }
    
    init(_ configuration: Realm.Configuration, extraPredicates: [NSPredicate])
    
    func initDefaultValues()
}

protocol DaoProtocol: AnyDao, ReactiveCompatible {
    associatedtype T: RealmConvertible
    typealias R = T.RealmObjectType
    
    // MARK: Create or update
    
    func createOrUpdate(_ object: T, write: Bool) -> Error?
    func createOrUpdate(_ objects: [T], write: Bool) -> Error?
    
    // MARK: Find
    
    func findOne(uid: T.Uid, write: Bool) -> T?
    
    func findOne(where predicate: NSPredicate?,
                 sortBy sortDescriptors: [SortDescriptor]?,
                 write: Bool) -> T?
    
    func find(where predicate: NSPredicate?,
              sortBy sortDescriptors: [SortDescriptor]?,
              write: Bool) -> [T]
    
    // MARK: Delete
    
    func deleteOne(_ object: T, write: Bool) -> Error?
    func delete(_ objects: [T], write: Bool) -> Error?
    func delete(where predicate: NSPredicate?,
                sortBy sortDescriptors: [SortDescriptor]?,
                write: Bool) -> Error?
}

extension DaoProtocol {
    
    func findOne(uid: T.Uid) -> T? {
        return findOne(uid: uid, write: false)
    }
    
    func createOrUpdate(_ object: T) -> Error? {
        return createOrUpdate(object, write: true)
    }
    
    func createOrUpdate(_ objects: [T]) -> Error? {
        return createOrUpdate(objects, write: true)
    }
    
    func deleteOne(_ object: T) -> Error? {
        return deleteOne(object, write: true)
    }
    
    func delete(_ objects: [T]) -> Error? {
        return delete(objects, write: true)
    }
    
    func clear() {
        return realm(write: true) { realm in
            realm.delete(realm.objects(R.self))
        }
    }
}

extension DaoProtocol {
    
    func query(_ realm: Realm, where predicate: NSPredicate? = nil) -> RealmQuery<T> {
        return RealmQuery<T>(realm, where: predicate)
    }
    
    func realm(write: Bool = false, action: (Realm) throws -> Void) {
        realm(onError: { _ in () }, write: write, action: action)
    }
    
    func realm(write: Bool = false, action: (Realm) throws -> Void) -> Error? {
        return realm(onError: { $0 }, write: write, action: { (realm: Realm) -> Error? in
            try action(realm)
            return nil
        })
    }
    
    func realm<R>(or defaultValue: R, write: Bool = false, action: (Realm) throws -> R) -> R {
        return realm(onError: { _ in defaultValue }, write: write, action: action)
    }
    
    func realm<R>(onError errorHandler: (Error) -> R, write: Bool = false, action: (Realm) throws -> R) -> R {
        return lock.sync {
            return realmAction(onError: errorHandler, write: write, action: action)
        }
    }
    
    // MARK: Realm Actions
    
    private func realmAction<R>(or defaultValue: R, write: Bool = false, action: (Realm) throws -> R) -> R {
        return realmAction(onError: { _ in defaultValue }, write: write, action: action)
    }
    
    private func realmAction<R>(onError errorHandler: (Error) -> R, write: Bool = false, action: (Realm) throws -> R) -> R {
        do {
            let realm = try Realm(configuration: configuration)
            realm.refresh()
            guard write else {
                return try action(realm)
            }
            var result: R?
            try realm.write { result = try action(realm) }
            if let result = result {
                return result
            }
            else {
                let error = "Invalid \(#function) result"
                print("__REALM_ERROR ❗️ \(self) ", error)
                return errorHandler(error)
            }
        } catch let error {
            print("__REALM_ERROR ❗️ \(self) ", error)
            return errorHandler(error)
        }
    }
}

extension String: Error {}
