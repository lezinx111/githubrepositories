//
//  Dao+Rx.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxCocoa
import RxRealm

extension Reactive where Base: DaoProtocol {
    
    var all: Observable<[Base.T]> {
        realm()
    }
    
    func `where`(_ predicate: NSPredicate,
                 sort: [SortDescriptor] = [],
                 synchronousStart: Bool = true,
                 scheduler: SchedulerType = MainScheduler.instance) -> Observable<[Base.T]> {
        realm(query: { $0.query($1).where(predicate: predicate).where(predicate: $0.extraPredicates.andPredicate).sort(by: sort) },
                     synchronousStart: synchronousStart,
                     scheduler: scheduler)
    }
    
    func realm(query: (Base, Realm) -> RealmQuery<Base.T> = { RealmQuery($1).where(predicate: $0.extraPredicates.andPredicate) },
               sort: [SortDescriptor] = [],
               synchronousStart: Bool = true,
               scheduler: SchedulerType? = MainScheduler.instance) -> Observable<[Base.T]> {
        do {
            let realm = try Realm(configuration: base.configuration)
            let q = query(base, realm).sort(by: sort)
            let observable = Observable.collection(from: q.results(), synchronousStart: synchronousStart)
                .map { Array($0) }
                .map { $0.compactMap { try? Base.T(realm: $0) } }
            if let scheduler = scheduler {
                return observable.observeOn(scheduler)
            }
            return observable
        } catch {
            print("__REALM_ERROR ❗️ \(self) \(error)")
            return .error(error)
        }
    }
}
