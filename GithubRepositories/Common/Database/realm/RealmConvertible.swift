//
//  RealmConvertible.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

struct RealmConversionError: Error {
    enum Kind {
        case decode
        case encode
    }
    
    var kind: Kind
    var underlyingError: Error?
    var debugDescription: String?
    
    init(_ kind: Kind, debugDescription: String? = nil, underlyingError: Error? = nil) {
        self.kind = kind
        self.debugDescription = debugDescription
        self.underlyingError = underlyingError
    }
}

extension RealmConvertible where Self: Codable, RealmObjectType: Codable {
    
    init(realm: RealmObjectType) throws {
        let decoder = RealmDecoder()
        do {
            self = try decoder.decode(Self.self, from: realm)
        } catch let error as RealmConversionError {
            #if DEBUG
            var debugTrace: String = ""
            dump(realm, to: &debugTrace)
            print("🗄 REALM_DECODING_ERROR (\(RealmObjectType.self) -> \(Self.self)): \(error) \(debugTrace)")
            #endif
            throw error
        } catch {
            #if DEBUG
            var debugTrace: String = ""
            dump(realm, to: &debugTrace)
            print("🗄 REALM_DECODING_ERROR (\(RealmObjectType.self) -> \(Self.self)): \(error) \(debugTrace)")
            #endif
            let errorMessage = "Failed to decode \(RealmObjectType.self) -> \(Self.self)"
            throw RealmConversionError(.decode, debugDescription: errorMessage, underlyingError: error)
        }
    }
    
    func realmObject() throws -> RealmObjectType {
        let encoder = RealmEncoder()
        do {
            return try encoder.encode(self)
        }  catch let error as RealmConversionError {
            #if DEBUG
            var debugTrace: String = ""
            dump(self, to: &debugTrace)
            print("🗄 REALM_ENCODING_ERROR (\(Self.self) -> \(RealmObjectType.self)): \(error) \(debugTrace)")
            #endif
            throw error
        } catch {
            #if DEBUG
            var debugTrace: String = ""
            dump(self, to: &debugTrace)
            print("🗄 REALM_ENCODING_ERROR (\(Self.self) -> \(RealmObjectType.self)): \(error) \(debugTrace)")
            #endif
            let errorMessage = "Failed to encode \(Self.self) -> \(RealmObjectType.self)"
            throw RealmConversionError(.encode, debugDescription: errorMessage, underlyingError: error)
        }
    }
}

protocol AnyRealmConvertible {
    var uid: String { get }

    init(anyRealm: RealmSwift.Object) throws

    func anyRealmObject() -> RealmSwift.Object
}

protocol RealmConvertible: AnyRealmConvertible, Unique where RealmObjectType: RealmSwift.Object {
    associatedtype RealmObjectType
    
    init(realm: RealmObjectType) throws

    func realmObject() throws -> RealmObjectType
}

extension RealmConvertible {
    init(anyRealm: RealmSwift.Object) throws {
        guard let realm = anyRealm as? RealmObjectType else {
            let error = "\(Self.self): invalid realm object \(anyRealm) instead of \(RealmObjectType.self)"
            throw RealmConversionError(.decode, debugDescription: error)
        }
        try self.init(realm: realm)
     }
    
    func anyRealmObject() -> RealmSwift.Object {
        return RealmObjectType()
    }
}

extension Sequence where Element: RealmCollectionValue {
    func realmList() -> RealmSwift.List<Element> {
        let list = RealmSwift.List<Element>()
        forEach { list.append($0) }
        return list
    }
}

extension Array where Element: RealmConvertible {
    func realmObjects() throws -> [Element.RealmObjectType] {
        return compactMap { try? $0.realmObject() }
    }
}

extension Realm {
    func delete<T: RealmConvertible>(_ object: T) throws {
        delete(try object.realmObject())
    }
    
    func delete<T: RealmConvertible>(_ objects: [T]) throws {
        delete(try objects.realmObjects())
    }
    
    func createOrUpdate<T: RealmConvertible>(_ object: T) throws {
        add(try object.realmObject(), update: .all)
    }
    
    func createOrUpdate<T: RealmConvertible>(_ objects: [T]) throws {
        add(try objects.realmObjects(), update: .all)
    }
    
    func add<T: RealmConvertible>(_ object: T, update: Realm.UpdatePolicy = .all) throws {
        add(try object.realmObject(), update: update)
    }
    
    func add<T: RealmConvertible>(_ objects: [T], update: Realm.UpdatePolicy = .all) throws {
        add(try objects.realmObjects(), update: update)
    }
}
