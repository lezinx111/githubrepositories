//
//  Unique.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

protocol Unique {
    associatedtype Uid: Hashable
    var uid: Uid { get }
}
