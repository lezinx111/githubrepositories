//
//  RealmEncoder.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

typealias RealmList = RealmSwift.List

class RealmEncoder {
    
    struct Options {
        var userInfo: [CodingUserInfoKey: Any]
    }
    
    var options: Options
    
    init() {
        self.options = Options(userInfo: [:])
    }
    
    open func encode<T: Encodable & RealmConvertible>(_ value: T) throws -> T.RealmObjectType {
        let encoder = _RealmEncoder(options: options)
        encoder.objects.append(value.anyRealmObject())
        try value.encode(to: encoder)
        let object = encoder.objects.removeLast()
        if let primaryKey = T.RealmObjectType.primaryKey() {
            object[primaryKey] = value.uid
        }
        return object as! T.RealmObjectType
    }
}

private class _RealmEncoder: Encoder {
    var options: RealmEncoder.Options
    var codingPath: [CodingKey]
    var objects: [Object] = []
    var singleValue: Any?
    var userInfo: [CodingUserInfoKey: Any] {
        return options.userInfo
    }
    
    init(options: RealmEncoder.Options,
         codingPath: [CodingKey] = []) {
        self.options = options
        self.codingPath = codingPath
    }
    
    func container<Key>(keyedBy type: Key.Type) -> KeyedEncodingContainer<Key> where Key: CodingKey {
        let container = RealmKeyedEncodingContainer<Key>(encoder: self)
        return KeyedEncodingContainer(container)
    }
    
    func unkeyedContainer() -> UnkeyedEncodingContainer {
        return RealmUnkeyedEncodingContainer(encoder: self)
    }
    
    func singleValueContainer() -> SingleValueEncodingContainer {
        return RealmSingleValueEncodingContainer(encoder: self)
    }
}

// MARK: - RealmUnkeyedEncodingContainer

private struct RealmUnkeyedEncodingContainer: UnkeyedEncodingContainer {
    mutating func encodeNil() throws {
        // skip nil values
        count += 1
    }
    
    mutating func encode(_ value: Bool) throws { try encode_(value) }
    mutating func encode(_ value: String) throws { try encode_(value) }
    mutating func encode(_ value: Double) throws { try encode_(value) }
    mutating func encode(_ value: Float) throws { try encode_(value) }
    mutating func encode(_ value: Int) throws { try encode_(value) }
    mutating func encode(_ value: Int8) throws { try encode_(value) }
    mutating func encode(_ value: Int16) throws { try encode_(value) }
    mutating func encode(_ value: Int32) throws { try encode_(value) }
    mutating func encode(_ value: Int64) throws { try encode_(value) }

    mutating func encode(_ value: UInt) throws { try encode_(Int(clamping: value)) }
    mutating func encode(_ value: UInt8) throws { try encode_(Int16(clamping: value)) }
    mutating func encode(_ value: UInt16) throws { try encode_(Int32(clamping: value)) }
    mutating func encode(_ value: UInt32) throws { try encode_(Int64(clamping: value)) }
    mutating func encode(_ value: UInt64) throws { try encode_(Int64(clamping: value)) }
    
    mutating func encode<T>(_ value: T) throws where T: Encodable {
        if let date = value as? Date {
            try encode_(date)
        }
        else if let data = value as? Data {
            try encode_(data)
        }
        else if let realmConvertible = value as? AnyRealmConvertible {
            let realmObject = realmConvertible.anyRealmObject()
            if let primaryKey = type(of: realmObject).primaryKey() {
                realmObject[primaryKey] = realmConvertible.uid
            }
            encoder.objects.append(realmObject)
            try value.encode(to: encoder)
            encoder.objects.removeLast()
            try encodeObject(realmObject)
        }
        else {
            encoder.singleValue = nil
            try value.encode(to: encoder)
            if let singleValue = encoder.singleValue {
                encoder.singleValue = nil
                switch singleValue {
                case let value as Bool:     try encode_(value)
                case let value as String:   try encode_(value)
                case let value as Double:   try encode_(value)
                case let value as Float:    try encode_(value)
                case let value as Int:      try encode_(value)
                case let value as Int8:     try encode_(value)
                case let value as Int16:    try encode_(value)
                case let value as Int32:    try encode_(value)
                case let value as Int64:    try encode_(value)
                case let value as UInt:     try encode_(Int(clamping: value))
                case let value as UInt8:    try encode_(Int16(value))
                case let value as UInt16:   try encode_(Int32(value))
                case let value as UInt32:   try encode_(Int64(value))
                case let value as UInt64:   try encode_(Int64(clamping: value))
                default:
                     throw EncodingError.invalidValue(value, .init(codingPath: codingPath, debugDescription: "Invalid value"))
                }
            }
        }
    }
    
    private mutating func encode_<T: RealmCollectionValue>(_ value: T) throws {
        let key = encoder.codingPath.last!.stringValue
        let list = encoder.objects.last![key] as! RealmList<T>
        list.append(value)
        count += 1
    }
    
    private mutating func encodeObject(_ value: AnyObject) throws {
        let key = encoder.codingPath.last!.stringValue
        let list = encoder.objects.last![key] as! ListBase
        list._rlmArray.add(value)
        count += 1
    }
    
    var encoder: _RealmEncoder
    var codingPath: [CodingKey] {
        return encoder.codingPath
    }
    var count: Int = 0
    
    init(encoder: _RealmEncoder) {
        self.encoder = encoder
        
    }
    
    mutating func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type) -> KeyedEncodingContainer<NestedKey> where NestedKey: CodingKey {
        fatalError("\(self) \(#function) \(RealmEncoder.self) doesnt support nested containers \(keyType)")
    }
    
    mutating func nestedUnkeyedContainer() -> UnkeyedEncodingContainer {
        return RealmUnkeyedEncodingContainer(encoder: encoder)
    }
    
    mutating func superEncoder() -> Encoder {
        return encoder
    }
}

// MARK: - RealmSingleValueContainer

private struct RealmSingleValueEncodingContainer: SingleValueEncodingContainer {
    
    var encoder: _RealmEncoder
    var encodedValue = false
    var codingPath: [CodingKey] {
        return encoder.codingPath
    }
    
    init(encoder: _RealmEncoder) {
        self.encoder = encoder
    }
    
    mutating func encodeNil() throws { try encode_(NSNull()) }
    
    mutating func encode(_ value: Bool) throws { try encode_(value) }
    mutating func encode(_ value: String) throws { try encode_(value) }
    mutating func encode(_ value: Double) throws { try encode_(value) }
    mutating func encode(_ value: Float) throws { try encode_(value) }
    mutating func encode(_ value: Int) throws { try encode_(value) }
    mutating func encode(_ value: Int8) throws { try encode_(value) }
    mutating func encode(_ value: Int16) throws { try encode_(value) }
    mutating func encode(_ value: Int32) throws { try encode_(value) }
    mutating func encode(_ value: Int64) throws { try encode_(value) }
    
    mutating func encode(_ value: UInt) throws { try encode_(Int(clamping: value)) }
    mutating func encode(_ value: UInt8) throws { try encode_(Int16(value)) }
    mutating func encode(_ value: UInt16) throws { try encode_(Int32(value)) }
    mutating func encode(_ value: UInt32) throws { try encode_(Int64(value)) }
    mutating func encode(_ value: UInt64) throws { try encode_(Int64(clamping: value)) }

    mutating func encode<T>(_ value: T) throws where T: Encodable {
        if value is AnyRealmConvertible {
            try assertNotEncoded(value)
            try value.encode(to: encoder)
            encodedValue = true
        }
        else {
            try encode_(value)
        }
    }

    private mutating func encode_<T>(_ value: T) throws {
        try assertNotEncoded(value)
        encoder.singleValue = value
        encodedValue = true
    }
    
    private func assertNotEncoded<T>(_ value: T) throws {
        guard !encodedValue else {
            let error = "\(RealmEncoder.self) \(self) container is already encoded"
            throw EncodingError.invalidValue(value, .init(codingPath: codingPath, debugDescription: error))
        }
    }
}

// MARK: - RealmKeyedEncodingContainer

private struct RealmKeyedEncodingContainer<K: CodingKey>: KeyedEncodingContainerProtocol {
    typealias Key = K
    
    var encoder: _RealmEncoder
    var codingPath: [CodingKey] {
        return encoder.codingPath
    }
    
    mutating func encodeNil(forKey key: K) throws { encoder.objects.last![key.stringValue] = nil }
 
    mutating func encode(_ value: Bool, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: String, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: Double, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: Float, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: Int, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: Int8, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: Int16, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: Int32, forKey key: K) throws { try self.encode_(value, forKey: key) }
    mutating func encode(_ value: Int64, forKey key: K) throws { try self.encode_(value, forKey: key) }
    
    mutating func encode(_ value: UInt, forKey key: K) throws { try self.encode_(Int(clamping: value), forKey: key) }
    mutating func encode(_ value: UInt8, forKey key: K) throws { try self.encode_(Int16(value), forKey: key) }
    mutating func encode(_ value: UInt16, forKey key: K) throws { try self.encode_(Int32(value), forKey: key) }
    mutating func encode(_ value: UInt32, forKey key: K) throws { try self.encode_(Int64(value), forKey: key) }
    mutating func encode(_ value: UInt64, forKey key: K) throws { try self.encode_(Int64(clamping: value), forKey: key) }
    
    mutating func encode<T>(_ value: T, forKey key: K) throws where T: Encodable {
        if T.self == Data.self || T.self == Date.self {
            encoder.objects.last![key.stringValue] = value
        }
        else {
            encoder.codingPath.append(key)
            defer { encoder.codingPath.removeLast() }
            
            if let realmConvertible = value as? AnyRealmConvertible {
                encoder.objects.append(realmConvertible.anyRealmObject())
                do {
                    try value.encode(to: encoder)
                    let object = encoder.objects.removeLast()
                    
                    if let primaryKey = type(of: object).primaryKey() {
                        object[primaryKey] = realmConvertible.uid
                    }
                    encoder.objects.last![key.stringValue] = object
                } catch {
                    encoder.objects.removeLast()
                }
                
            }
            else {
                // Value is either array or custom decodable (enum)
                encoder.singleValue = nil
                try value.encode(to: encoder)
                if let singleValue = encoder.singleValue {
                    if singleValue is NSNull {
                        encoder.objects.last![key.stringValue] = nil
                    }
                    else {
                        encoder.objects.last![key.stringValue] = singleValue
                    }
                    encoder.singleValue = nil
                }
            }
        }
    }
    
    func encode_<T: Encodable, K: CodingKey>(_ value: T, forKey key: K) throws {
        encoder.objects.last![key.stringValue] = value
    }
    
    mutating func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type, forKey key: K) -> KeyedEncodingContainer<NestedKey> where NestedKey: CodingKey {
        fatalError("\(self) \(#function) \(RealmEncoder.self) doesnt support nested containers \(keyType)")
    }
    
    mutating func nestedUnkeyedContainer(forKey key: K) -> UnkeyedEncodingContainer {
        return RealmUnkeyedEncodingContainer(encoder: encoder)
    }
    
    mutating func superEncoder() -> Encoder {
        return encoder
    }
    
    mutating func superEncoder(forKey key: K) -> Encoder {
        return encoder
    }
}
