//
//  RealmService.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift

class RealmService {
    
    let configuration: Realm.Configuration
    let realm: Realm?
    
    let githubRepository: DaoGithubRepository
    
    init() {
        self.configuration = RealmService.makeConfig()
        self.realm = try? Realm(configuration: configuration)
        
        self.githubRepository = DaoGithubRepository(configuration, extraPredicates: [])
    }
    
    func initDefaultValues() {
        [AnyDao](arrayLiteral:
                    githubRepository
        ).forEach { $0.initDefaultValues() }
    }
    
    func clear() {
        githubRepository.clear()

    }
    
    private static func makeConfig() -> Realm.Configuration {
        
        let realmKeyConfiguration = RealmKeyConfiguration()
        let newSchemaVersion: UInt64 = 0
        
        return Realm.Configuration(
            encryptionKey: realmKeyConfiguration.getKey() as Data,
            schemaVersion: newSchemaVersion,
            migrationBlock: { (migration: Migration, oldSchemaVersion) in
                RealmService.migrateRealm(migration: migration,
                                          oldVersion: oldSchemaVersion,
                                          newVersion: newSchemaVersion)
            },
            deleteRealmIfMigrationNeeded: false)
    }
    
    private static func migrateRealm(migration: Migration, oldVersion: UInt64, newVersion: UInt64) {
    }
}
