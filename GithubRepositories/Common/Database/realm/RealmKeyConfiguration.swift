//
//  RealmKeyConfiguration.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

class RealmKeyConfiguration {
    
    func getKey() -> NSData {
           // Identifier for our keychain entry - should be unique for your application
        let keychainIdentifier = "io.GithubRepositories"
           let keychainIdentifierData = keychainIdentifier.data(using: String.Encoding.utf8, allowLossyConversion: false)!

           // First check in the keychain for an existing key
           var query: [NSString: AnyObject] = [
               kSecClass: kSecClassKey,
               kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
               kSecAttrKeySizeInBits: 512 as AnyObject,
               kSecReturnData: true as AnyObject
           ]

           // To avoid Swift optimization bug, should use withUnsafeMutablePointer() function to retrieve the keychain item
           // See also: http://stackoverflow.com/questions/24145838/querying-ios-keychain-using-swift/27721328#27721328
           var dataTypeRef: AnyObject?
           var status = withUnsafeMutablePointer(to: &dataTypeRef) { SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0)) }
           if status == errSecSuccess {
               return dataTypeRef as! NSData
           }

           // No pre-existing key from this application, so generate a new one
           let keyData = NSMutableData(length: 64)!
           let result = SecRandomCopyBytes(kSecRandomDefault, 64, keyData.mutableBytes.bindMemory(to: UInt8.self, capacity: 64))
           assert(result == 0, "Failed to get random bytes")

           // Store the key in the keychain
           query = [
               kSecClass: kSecClassKey,
               kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
               kSecAttrKeySizeInBits: 512 as AnyObject,
               kSecValueData: keyData
           ]

           status = SecItemAdd(query as CFDictionary, nil)
           assert(status == errSecSuccess, "Failed to insert the new key in the keychain")

           return keyData
       }
}
