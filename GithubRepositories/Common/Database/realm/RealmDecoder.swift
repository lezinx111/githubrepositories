//
//  RealmDecoder.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class RealmDecoder {
    
    struct Options {
        var userInfo: [CodingUserInfoKey: Any]
    }
    
    var options: Options
    
    init() {
        self.options = Options(userInfo: [:])
    }

    func decode<T: Decodable & RealmConvertible>(_ type: T.Type, from realmObject: T.RealmObjectType) throws -> T {
        let decoder = _RealmDecoder(options: options)
        decoder.objects.append(realmObject)
        return try T(from: decoder)
    }
}

private class _RealmDecoder: Decoder {
    var options: RealmDecoder.Options
    var codingPath: [CodingKey]
    var objects: [RealmSwift.Object] = []
    var singleValue: Any?
    var userInfo: [CodingUserInfoKey: Any] {
        return options.userInfo
    }
    
    init(options: RealmDecoder.Options,
         codingPath: [CodingKey] = []) {
        self.options = options
        self.codingPath = codingPath
    }
    
    func unbox<T>(_ value: Any?, as type: T.Type) throws -> T {
        guard let unboxed = value as? T else {
            throw DecodingError.typeMismatch(type, .init(codingPath: self.codingPath, debugDescription: "Unable to decode \(String(describing: value)) as \(T.self)"))
        }
        return unboxed
    }
    
    func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key: CodingKey {
        let container = RealmKeyedDecodingContainer<Key>(decoder: self)
        return KeyedDecodingContainer(container)
    }
    
    func unkeyedContainer() throws -> UnkeyedDecodingContainer {
        return RealmUnkeyedDecodingContainer(decoder: self)
    }
    
    func singleValueContainer() throws -> SingleValueDecodingContainer {
        return RealmSingleValueDecodingContainer(decoder: self)
    }
}

private struct RealmKey: CodingKey {
    /// The string to use in a named collection (e.g. a string-keyed dictionary).
    var stringValue: String
    var intValue: Int?
    
    init?(stringValue: String) {
        self.stringValue = stringValue
        self.intValue = nil
    }
    
    init?(intValue: Int) {
        self.stringValue = intValue.description
        self.intValue = intValue
    }
    
    init(stringValue: String, intValue: Int) {
        self.stringValue = stringValue
        self.intValue = intValue
    }
}

// MARK: - RealmUnkeyedDecodingContainer

private struct RealmUnkeyedDecodingContainer: UnkeyedDecodingContainer {
    var decoder: _RealmDecoder
    var currentIndex: Int
    var codingPath: [CodingKey] {
        return decoder.codingPath
    }
    
    var count: Int? {
        let key = decoder.codingPath.last!.stringValue
        let list = decoder.objects.last![key] as! ListBase
        return list.count
    }
    
    var isAtEnd: Bool {
        return currentIndex >= count!
    }
    
    init(decoder: _RealmDecoder) {
        self.decoder = decoder
        self.currentIndex = 0
    }
    
    mutating func decodeNil() throws -> Bool {
        let key = decoder.codingPath.last!.stringValue
        let list = decoder.objects.last![key] as! ListBase
        let value = list._rlmArray.object(at: UInt(currentIndex))
        let isNil = value is NSNull
        if isNil {
            currentIndex += 1
        }
        return isNil
    }
    
    mutating func decode(_ type: Bool.Type) throws -> Bool { return try decode_(Bool.self) }
    mutating func decode(_ type: String.Type) throws -> String { return try decode_(String.self) }
    mutating func decode(_ type: Double.Type) throws -> Double { return try decode_( Double.self) }
    mutating func decode(_ type: Float.Type) throws -> Float { return try decode_(Float.self) }
    mutating func decode(_ type: Int.Type) throws -> Int { return try decode_(Int.self) }
    mutating func decode(_ type: Int8.Type) throws -> Int8 { return try decode_(Int8.self) }
    mutating func decode(_ type: Int16.Type) throws -> Int16 { return try decode_(Int16.self) }
    mutating func decode(_ type: Int32.Type) throws -> Int32 { return try decode_(Int32.self) }
    mutating func decode(_ type: Int64.Type) throws -> Int64 { return try decode_(Int64.self) }
    mutating func decode(_ type: UInt.Type) throws -> UInt { return try decode_(UInt.self) }
    
    mutating func decode(_ type: UInt8.Type) throws -> UInt8 { return UInt8(try decode_(Int16.self)) }
    mutating func decode(_ type: UInt16.Type) throws -> UInt16 { return UInt16(try decode_(Int32.self)) }
    mutating func decode(_ type: UInt32.Type) throws -> UInt32 { return UInt32(try decode_(Int64.self)) }
    mutating func decode(_ type: UInt64.Type) throws -> UInt64 { return UInt64(try decode_(Int64.self)) }
    
    mutating func decode<T>(_ type: T.Type) throws -> T where T: Decodable {
        let key = decoder.codingPath.last!.stringValue
        let list = decoder.objects.last![key] as! ListBase
        let value = list._rlmArray.object(at: UInt(currentIndex))
        let decoded: T
        if let object = value as? RealmSwift.Object {
            decoder.objects.append(object)
            defer { decoder.objects.removeLast() }
            decoded = try T(from: decoder)
        }
        else if type == Data.self || type == Date.self {
            decoded = try decoder.unbox(value, as: type)
        }
        else {
            let key = RealmKey(stringValue: key, intValue: currentIndex)
            decoder.codingPath.append(key)
            defer { decoder.codingPath.removeLast() }
            decoded = try T(from: decoder)
        }
        currentIndex += 1
        return decoded
    }
    
    private mutating func decode_<T>(_ type: T.Type) throws -> T {
        let key = decoder.codingPath.last!.stringValue
        let list = decoder.objects.last![key] as! ListBase
        let value = list._rlmArray.object(at: UInt(currentIndex))
        let unboxed = try decoder.unbox(value, as: type)
        currentIndex += 1
        return unboxed
    }
    
    mutating func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type) throws -> KeyedDecodingContainer<NestedKey> where NestedKey: CodingKey {
        fatalError("\(self) \(#function) \(RealmDecoder.self) doesnt support nested containers \(type)")
    }
    
    mutating func nestedUnkeyedContainer() throws -> UnkeyedDecodingContainer {
        return RealmUnkeyedDecodingContainer(decoder: decoder)
    }
    
    mutating func superDecoder() throws -> Decoder {
        return decoder
    }
}

// MARK: - RealmSingleValueDecodingContainer

private struct RealmSingleValueDecodingContainer: SingleValueDecodingContainer {
    var decoder: _RealmDecoder
    var encodedValue = false
    var codingPath: [CodingKey] {
        return decoder.codingPath
    }
    
    init(decoder: _RealmDecoder) {
        self.decoder = decoder
    }
    
    func decodeNil() -> Bool {
        let key = decoder.codingPath.last!.stringValue
        let value = decoder.objects.last![key]
        return value == nil || value is NSNull
    }
    
    func decode(_ type: Bool.Type) throws -> Bool { return try decode_(Bool.self) }
    func decode(_ type: String.Type) throws -> String { return try decode_(String.self) }
    func decode(_ type: Double.Type) throws -> Double { return try decode_( Double.self) }
    func decode(_ type: Float.Type) throws -> Float { return try decode_(Float.self) }
    func decode(_ type: Int.Type) throws -> Int { return try decode_(Int.self) }
    func decode(_ type: Int8.Type) throws -> Int8 { return try decode_(Int8.self) }
    func decode(_ type: Int16.Type) throws -> Int16 { return try decode_(Int16.self) }
    func decode(_ type: Int32.Type) throws -> Int32 { return try decode_(Int32.self) }
    func decode(_ type: Int64.Type) throws -> Int64 { return try decode_(Int64.self) }
    func decode(_ type: UInt.Type) throws -> UInt { return try decode_(UInt.self) }
    
    func decode(_ type: UInt8.Type) throws -> UInt8 { return UInt8(try decode_(Int16.self)) }
    func decode(_ type: UInt16.Type) throws -> UInt16 { return UInt16(try decode_(Int32.self)) }
    func decode(_ type: UInt32.Type) throws -> UInt32 { return UInt32(try decode_(Int64.self)) }
    func decode(_ type: UInt64.Type) throws -> UInt64 { return UInt64(try decode_(Int64.self)) }
    
    func decode<T>(_ type: T.Type) throws -> T where T: Decodable {
        if let codingKey = decoder.codingPath.last {
            let key = codingKey.stringValue
            if let index = codingKey.intValue {
                let list = decoder.objects.last![key] as! ListBase
                let value = list._rlmArray.object(at: UInt(index))
                return try decodeObjectValue(value, as: type)
            }
            return try decodeObjectValue(decoder.objects.last![key], as: type)
        }
        else {
            // No path is given, trying to decode a root a object
            return try T(from: decoder)
        }
    }
    
    func decode_<T>(_ type: T.Type) throws -> T {
        let codingKey = decoder.codingPath.last!
        let key = codingKey.stringValue
        if let index = codingKey.intValue {
            let list = decoder.objects.last![key] as! ListBase
            let value = list._rlmArray.object(at: UInt(index))
            return try decodePrimitiveValue(value, as: type)
        }
        return try decodePrimitiveValue(decoder.objects.last![key], as: type)
    }
    
    func decodePrimitiveValue<T>(_ any: Any?, as type: T.Type) throws -> T {
        return try decoder.unbox(any, as: type)
    }
    
    func decodeObjectValue<T: Decodable>(_ any: Any?, as type: T.Type) throws -> T {
        if let object = any as? RealmSwift.Object {
            decoder.objects.append(object)
            defer { decoder.objects.removeLast() }
            return try T(from: decoder)
        }
        else if type == Decimal.self {
            let string = try decodePrimitiveValue(any, as: String.self)
            guard let decimal = Decimal(string: string, locale: Locale(identifier: "en_US")) else {
                throw DecodingError.typeMismatch(type, .init(codingPath: self.codingPath, debugDescription: "Unable to decode \(Decimal.self) from \(string)"))
            }
            return decimal as! T
        }
        else if type == Data.self || type == Date.self {
            return try decodePrimitiveValue(any, as: type)
        }
        else {
            return try T(from: decoder)
        }
    }
}

// MARK: - RealmKeyedDecodingContainer

private struct RealmKeyedDecodingContainer<K: CodingKey>: KeyedDecodingContainerProtocol {
    typealias Key = K
    
    var decoder: _RealmDecoder
    var codingPath: [CodingKey] {
        return decoder.codingPath
    }
    var allKeys: [K]
    
    init(decoder: _RealmDecoder) {
        self.decoder = decoder
        self.allKeys = []
    }
    
    func contains(_ key: K) -> Bool {
        let entry = decoder.objects.last![key.stringValue]
        return entry != nil && !(entry is NSNull)
    }
    
    func decodeNil(forKey key: K) throws -> Bool {
        let entry = decoder.objects.last![key.stringValue]
        return entry == nil || entry is NSNull
    }
    
    func decode(_ type: Bool.Type, forKey key: K) throws -> Bool { return try decode_(Bool.self, forKey: key) }
    func decode(_ type: String.Type, forKey key: K) throws -> String { return try decode_(String.self, forKey: key) }
    func decode(_ type: Double.Type, forKey key: K) throws -> Double { return try decode_(Double.self, forKey: key) }
    func decode(_ type: Float.Type, forKey key: K) throws -> Float { return try decode_(Float.self, forKey: key) }
    func decode(_ type: Int.Type, forKey key: K) throws -> Int { return try decode_(Int.self, forKey: key) }
    func decode(_ type: Int8.Type, forKey key: K) throws -> Int8 { return try decode_(Int8.self, forKey: key) }
    func decode(_ type: Int16.Type, forKey key: K) throws -> Int16 { return try decode_(Int16.self, forKey: key) }
    func decode(_ type: Int32.Type, forKey key: K) throws -> Int32 { return try decode_(Int32.self, forKey: key) }
    func decode(_ type: Int64.Type, forKey key: K) throws -> Int64 { return try decode_(Int64.self, forKey: key) }
    
    func decode(_ type: UInt.Type, forKey key: K) throws -> UInt { return UInt(try decode_(Int.self, forKey: key)) }
    func decode(_ type: UInt8.Type, forKey key: K) throws -> UInt8 { return UInt8(try decode_(Int16.self, forKey: key)) }
    func decode(_ type: UInt16.Type, forKey key: K) throws -> UInt16 { return UInt16(try decode_(Int32.self, forKey: key)) }
    func decode(_ type: UInt32.Type, forKey key: K) throws -> UInt32 { return UInt32(try decode_(Int64.self, forKey: key)) }
    func decode(_ type: UInt64.Type, forKey key: K) throws -> UInt64 { return UInt64(try decode_(Int.self, forKey: key)) }
    
    func decode<T>(_ type: T.Type, forKey key: K) throws -> T where T: Decodable {
        decoder.codingPath.append(key)
        defer { decoder.codingPath.removeLast() }
        if let object = decoder.objects.last![key.stringValue] as? RealmSwift.Object {
            decoder.objects.append(object)
            defer { decoder.objects.removeLast() }
            return try T(from: decoder)
        }
        else if type == Data.self || type == Date.self {
            return try decode_(type, forKey: key)
        }
        else {
            return try T(from: decoder)
        }
    }
    
    private func decode_<T>(_ type: T.Type, forKey key: K) throws -> T {
        let value = decoder.objects.last![key.stringValue]
        return try decoder.unbox(value, as: type)
    }
    
    func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: K) throws -> KeyedDecodingContainer<NestedKey> where NestedKey: CodingKey {
        fatalError("\(self) \(#function) \(RealmDecoder.self) doesnt support nested containers \(type)")
    }
    
    func nestedUnkeyedContainer(forKey key: K) throws -> UnkeyedDecodingContainer {
        return RealmUnkeyedDecodingContainer(decoder: decoder)
    }
    
    func superDecoder() throws -> Decoder {
        return decoder
    }
    
    func superDecoder(forKey key: K) throws -> Decoder {
        return decoder
    }
}
