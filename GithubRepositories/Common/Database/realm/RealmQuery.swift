//
//  RealmQuery.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift

class RealmQuery<T: RealmConvertible> {
    
    private var realm: Realm
    private var predicates = [NSPredicate]()
    private var sortDescriptors = [SortDescriptor]()

    init(_ realm: Realm, where predicate: NSPredicate? = nil) {
        self.realm = realm
        if let predicate = predicate {
            self.predicates.append(predicate)
        }
    }
    
    func `where`(_ format: String, _ args: CVarArg...) -> RealmQuery {
        let predicate = withVaList(args) { NSPredicate(format: format, arguments: $0) }
        predicates.append(predicate)
        return self
    }
    
    func `where`(predicate: NSPredicate?) -> RealmQuery {
        if let predicate = predicate {
            predicates.append(predicate)
        }
        return self
    }
    
    func and(_ format: String, _ args: CVarArg...) -> RealmQuery {
        let predicate = withVaList(args) { NSPredicate(format: format, arguments: $0) }
        predicates.append(predicate)
        return self
    }
    
    func and(predicate: NSPredicate) -> RealmQuery {
        return `where`(predicate: predicate)
    }
    
    func sort(by property: String, ascending: Bool = true) -> RealmQuery {
        sortDescriptors.append(SortDescriptor(keyPath: property, ascending: ascending))
        return self
    }
    
    func sort(by descriptors: [SortDescriptor]) -> RealmQuery {
        sortDescriptors.append(contentsOf: descriptors)
        return self
    }
    
    func results() -> RealmSwift.Results<T.RealmObjectType> {
        var results = realm.objects(T.RealmObjectType.self)
        if !predicates.isEmpty {
            let predicate = predicates.count == 1 ? predicates.first! : NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            results = results.filter(predicate)
        }
        if !sortDescriptors.isEmpty {
            results = results.sorted(by: sortDescriptors)
        }
        return results
    }
    
    func object(id: T.Uid) -> T.RealmObjectType? {
        return realm.object(ofType: T.RealmObjectType.self, forPrimaryKey: id)
    }
    
    func first() -> T.RealmObjectType? {
        return results().first
    }
    
    func array() -> [T.RealmObjectType] {
        return results().array()
    }
}

extension Sequence {
    func array() -> [Element] {
        return Array(self)
    }
}
