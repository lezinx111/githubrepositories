//
//  PThreadMutex.swift
//  GithubRepositories
//
//  Created by Ziong on 26.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

/// [More about mutexes](https://www.cocoawithlove.com/blog/2016/06/02/threads-and-mutexes.html)
/// [Inlined source](https://github.com/mattgallagher/CwlUtils/blob/master/Sources/CwlUtils/CwlMutex.swift)
/// A basic wrapper around the "NORMAL" and "RECURSIVE" `pthread_mutex_t` (a safe, general purpose FIFO mutex). This type is a "class" type to take advantage of the "deinit" method and prevent accidental copying of the `pthread_mutex_t`.

typealias Mutex = PThreadMutex

final class NSMutex {
    
    let lock: NSLocking
    
    init(recursive: Bool = true) {
        self.lock = recursive ? NSRecursiveLock() : NSLock()
    }
}

extension NSMutex {
    @discardableResult
    func sync<R>(execute work: () throws -> R) rethrows -> R {
        lock.lock(); defer { lock.unlock() }
        return try work()
    }
}

final class PThreadMutex {
    
    var unsafeMutex = pthread_mutex_t()
    
    init(recursive: Bool = true) {
        var attr = pthread_mutexattr_t()
        guard pthread_mutexattr_init(&attr) == 0 else {
            preconditionFailure()
        }
        
        pthread_mutexattr_settype(&attr, Int32(recursive ? PTHREAD_MUTEX_RECURSIVE : PTHREAD_MUTEX_NORMAL))
        
        guard pthread_mutex_init(&unsafeMutex, &attr) == 0 else {
            preconditionFailure()
        }
        pthread_mutexattr_destroy(&attr)
    }
    
    deinit {
        pthread_mutex_destroy(&unsafeMutex)
    }
}

extension PThreadMutex {
    @discardableResult
    func sync<R>(execute work: () throws -> R) rethrows -> R {
        pthread_mutex_lock(&unsafeMutex)
        defer { pthread_mutex_unlock(&unsafeMutex) }
        return try work()
    }
}
