//
//  NSObject+Extension.swift
//  Meow
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation

extension NSObject {
    class var nameOfClass: String {
        let className = NSStringFromClass(self)
        let index = className.index(after: className.lastIndex(of: ".")!)
        return String(className.suffix(from: index))
    }
}
