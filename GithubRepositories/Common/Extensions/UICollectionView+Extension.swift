//
//  UICollectionView+Extension.swift
//  Meow
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit

extension UICollectionView {
    func dequeueReusableCell<T: UICollectionViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: type.nameOfClass, for: indexPath) as! T
    }
    
    func register<T: UICollectionViewCell>(_ type: T.Type) {
        let name = type.nameOfClass
        register(type, forCellWithReuseIdentifier: name)
    }
}

