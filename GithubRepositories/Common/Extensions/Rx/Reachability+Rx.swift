//
//  Reachability+Rx.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import RxSwift

extension ObservableType where Element == Void {
    
    static func online() -> Observable<Void> {
        Observable.just(ReachabilityHelper.isOnline)
            .flatMap {
                $0 ? Observable.just(()) : .error(ApiError(errorType: .offline))
            }
    }
}
