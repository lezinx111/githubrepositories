//
//  UIView+Rx.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

extension Reactive where Base: UIView {
    
    var tapGestures: ControlEvent<UITapGestureRecognizer> {
        let tapGesture = UITapGestureRecognizer()
        base.addGestureRecognizer(tapGesture)
        return tapGesture.rx.event.asControlEvent()
    }
}
