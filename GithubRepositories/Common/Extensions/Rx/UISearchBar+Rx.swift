//
//  UISearchBar+Rx.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UISearchBar {
    var isLoading: Binder<Bool> {
        Binder(self.base) { view, isLoading in
            view.isLoading = isLoading
        }
    }
}
