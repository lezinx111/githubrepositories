//
//  String+Extensions.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit

extension String {
    func trimmed(_ charSet: CharacterSet = CharacterSet.whitespacesAndNewlines) -> String {
        return self.trimmingCharacters(in: charSet)
    }
    
    func height(for width: CGFloat,
                    attributes: [NSAttributedString.Key : Any]? = nil) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: attributes,
                                            context: nil)
        return ceil(boundingBox.height)
    }
}
