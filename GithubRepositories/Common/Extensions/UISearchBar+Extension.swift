//
//  UISearchBar+Extension.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import UIKit

extension UISearchBar {
    private var textField: UITextField? {
        let _subviews = self.subviews.flatMap { $0.subviews }
        return (_subviews.filter { $0 is UITextField }).first as? UITextField
    }
    
    private var activityIndicator: UIActivityIndicatorView? {
        if #available(iOS 13.0, *) {
            return searchTextField.leftView?.subviews.compactMap { $0 as? UIActivityIndicatorView }.first
        } else {
            return textField?.leftView?.subviews.compactMap{ $0 as? UIActivityIndicatorView }.first
        }
    }
    
    var isLoading: Bool {
        get {
            return activityIndicator != nil
        } set {
            if newValue {
                if activityIndicator == nil {
                    let _activityIndicator: UIActivityIndicatorView
                    if #available(iOS 13.0, *) {
                        _activityIndicator = UIActivityIndicatorView(style: .medium)
                    } else {
                        _activityIndicator = UIActivityIndicatorView(style: .gray)
                    }
                    _activityIndicator.startAnimating()
                    _activityIndicator.backgroundColor = UIColor.clear
                    if #available(iOS 13.0, *) {
                        searchTextField.leftView = _activityIndicator
                    } else {
                        setImage(#imageLiteral(resourceName: "transparentImage"), for: .search, state: .normal)
                        textField?.leftView?.addSubview(_activityIndicator)
                        let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero
                        _activityIndicator.center = CGPoint(x: leftViewSize.width / 2, y: leftViewSize.height / 2)
                    }
                }
            } else {
                if #available(iOS 13.0, *) {
                    searchTextField.leftView = UIImageView(image: UIImage(named: "searchIcon"))
                } else {
                    let _searchIcon = UIImage(named: "searchIcon")
                    self.setImage(_searchIcon, for: .search, state: .normal)
                    activityIndicator?.removeFromSuperview()
                }

            }
        }
    }
}
