//
//  RepositoryOwner.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift

class RepositoryOwner: Codable {
    var uid: String
    let login: String?
    let avatarUrl: String?
    
    init(login: String?, avatarUrl: String?) {
        self.login = login
        self.avatarUrl = avatarUrl
        self.uid = "\(login)_\(avatarUrl)"
    }
    
    
    enum CodingKeys: String, CodingKey {
           case login
           case avatarUrl
       }

    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.login = try container.decode(String.self, forKey: .login)
        self.avatarUrl = try container.decode(String.self, forKey: .avatarUrl)
        self.uid = "\(login)_\(avatarUrl)"
    }
}

extension RepositoryOwner: RealmConvertible {
    typealias RealmObjectType = RealmRepositoryOwner
    

}

@objcMembers
class RealmRepositoryOwner: RealmSwift.Object, Codable {
    override class func primaryKey() -> String? {
        return #keyPath(uid)
    }
    
    dynamic var uid: String = ""
    dynamic var login: String = ""
    dynamic var avatarUrl: String = ""
    
}


