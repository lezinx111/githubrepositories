//
//  GithubRepository.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import Foundation
import RealmSwift

class GithubRepository: Codable {
    var uid: String
    let owner: RepositoryOwner
    let htmlUrl: String?
    let stargazersCount: Int
    let fullName: String?
    var seen: Bool
    var dateAdded: Date
    
    init(htmlUrl: String?, owner: RepositoryOwner, fullName: String?, stargazersCount: Int, seen: Bool, dateAdded: Date) {
        self.uid = "\(htmlUrl)_\(stargazersCount)_\(owner.login)"
        self.owner = owner
        self.htmlUrl = htmlUrl
        self.stargazersCount = stargazersCount
        self.seen = seen
        self.fullName = fullName
        self.dateAdded = dateAdded
    }
    
    enum CodingKeys: String, CodingKey {
           case owner
           case htmlUrl
           case stargazersCount
           case fullName
       }

    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.owner = try container.decode(RepositoryOwner.self, forKey: .owner)
        self.htmlUrl = try container.decode(String.self, forKey: .htmlUrl)
        self.stargazersCount = try container.decode(Int.self, forKey: .stargazersCount)
        self.fullName = try container.decode(String.self, forKey: .fullName)
        self.dateAdded = .distantPast
        self.seen = false
        if let htmlUrl = htmlUrl {
            self.uid = "\(htmlUrl)"
        } else {
            fatalError()
            self.uid = ""
        }
        
    }

}

extension GithubRepository: RealmConvertible {
    typealias RealmObjectType = RealmGithubRepository
    typealias Uid = String
}

@objcMembers
class RealmGithubRepository: RealmSwift.Object, Codable {
    override class func primaryKey() -> String? {
        return #keyPath(uid)
    }
    
    @objc
    dynamic var uid: String = ""
    @objc
    dynamic var owner: RealmRepositoryOwner?
    @objc
    dynamic var htmlUrl = ""
    @objc
    dynamic var stargazersCount: Int = 0
    @objc
    dynamic var fullName = ""
    @objc
    dynamic var seen: Bool = false
    @objc
    dynamic var dateAdded: Date = .distantPast
    
}
