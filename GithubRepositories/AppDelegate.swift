//
//  AppDelegate.swift
//  GithubRepositories
//
//  Created by Ziong on 24.10.2020.
//  Copyright © 2020 Ziong. All rights reserved.
//

import UIKit
import OAuthSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AppereanceConfigurator.configure()
        Database.clear()
        Database.initDefaultValues()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if UserDefaults.standard.string(forKey: "apiToken") != nil {
            self.window?.rootViewController = DashboardViewController()
        } else {
            self.window?.rootViewController = UINavigationController(rootViewController: SignInViewController())
        }
        
        self.window?.makeKeyAndVisible()
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        OAuthSwift.handle(url: url)
        return true
    }

}

